let camera, scene, renderer;
let geometry, material, mesh;

function threeInit() {
  let width = $('#mapper').innerWidth();
  let height = $('#mapper').innerHeight();

	camera = new THREE.OrthographicCamera(
    width / -2, width / 2,
    height / 2, height / -2,
    -1, 1 );
	scene = new THREE.Scene();

  var quad_vertices =
  [
    -100.0,  100.0, 0.0,
     100.0,  100.0, 0.0,
     100.0, -100.0, 0.0,
    -100.0, -100.0, 0.0
  ];

  var quad_uvs =
  [
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0
  ];

  var quad_indices =
  [
    0, 2, 1, 0, 3, 2
  ];

  geometry = new THREE.BufferGeometry();
  vertices = new Float32Array( quad_vertices );
  uvs = new Float32Array( quad_uvs);
  indices = new Uint32Array( quad_indices )

  geometry.addAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );
  geometry.addAttribute( 'uv', new THREE.BufferAttribute( uvs, 2 ) );
  geometry.setIndex( new THREE.BufferAttribute( indices, 1 ) );

  const texture = new THREE.TextureLoader().load( 'assets/images/kogan-neural-synth.jpeg' );

  material = new THREE.MeshBasicMaterial( { map: texture } );
  //material.color = new THREE.Color( 0xfff000 );

	mesh = new THREE.Mesh( geometry, material );
	scene.add( mesh );

	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setSize( width, height );
	renderer.setAnimationLoop( animation );
	document.getElementById('mapper').appendChild( renderer.domElement );
}

function animation( time ) {
	mesh.rotation.z = time / 2000;
	renderer.render( scene, camera );
}

var panelHide = function(){

  // Store original panel width
  // to be used with panelShow()
  var panelWidth = $('#panel .content').css('width');
  panelWidth = parseInt(panelWidth, 10);

  $('#panel .toggle').hide();
  $('#panel .content').animate({
    left: -panelWidth
  }, 500, function(){
    $('#panel').data('hidden', true);
    $('#panel .toggle').css('left', 0);
    $('#panel .toggle').show();
  });
}

var panelShow = function(){
  $('#panel .toggle').hide();
  $('#panel .content').animate({
    left: 0
  }, 500, function(){
    $('#panel').data('hidden', false);

    var panelWidth = $('#panel .content').css('width');
    panelWidth = parseInt(panelWidth, 10);

    $('#panel .toggle').css('left', panelWidth);
    $('#panel .toggle').show();
  });
}

$(document).ready(function(){
  threeInit();

  $('#panel .toggle').click(function(){
    if ( $('#panel').data('hidden')  ){
      panelShow();
    } else {
      panelHide();
    }
  })
});
