# Pi Mapper Web GUI

Web-Based graphical user interface for [Pi Mapper](https://gitlab.com/kriwkrow/pimapper/).

## Development

Make sure you have Python 3 installed.

```
python3 --version
# Python 3.7.3
```

Install `virtualenv` using `pip3`

```
pip3 install virtualenv
```

Set up virtualenv in the ENV directory. Activate it.

```
virtualenv ENV
source ENV/bin/activate
```

Install Python requirements.

```
pip install -r requirements.txt
```

Run the app!

```
python webgui.py
```