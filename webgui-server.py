from flask import Flask, render_template
from flask_graphql import GraphQLView
from graphene import ObjectType, String, Schema

app = Flask(__name__, template_folder='./html', static_url_path='', static_folder='./html')
app.debug = True

@app.route('/')
def index():
    return render_template('index.html')

# GraphQL

class Query(ObjectType):
    # this defines a Field `hello` in our Schema with a single Argument `name`
    hello = String(name=String(default_value="stranger"))
    goodbye = String()

    # our Resolver method takes the GraphQL context (root, info) as well as
    # Argument (name) for the Field and returns data for the query Response
    def resolve_hello(root, info, name):
        return f'Hello {name}!'

    def resolve_goodbye(root, info):
        return 'See ya!'

schema = Schema(query=Query)

app.add_url_rule(
    '/gql', view_func=GraphQLView.as_view('graphql', schema=schema, graphiql=True)
)

if __name__ == '__main__':
    app.run()